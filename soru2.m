function ikinciBuyuk=soru2(A)
min=diziMin(A);
max=diziMax(A);
ikinciBuyuk=min;
dizininBoyutu=length(A);
for i=1:dizininBoyutu
    if (A(i)<max) && (A(i)>ikinciBuyuk)
        ikinciBuyuk=A(i);
    end
end