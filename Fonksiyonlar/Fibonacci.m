function sonuc=Fibonacci(indis)
if indis<1
    sonuc=-1;
elseif indis==1
    sonuc=0;
elseif indis==2
    sonuc=1;
else
    ikiOncekiSayi=0;
    birOncekiSayi=1;
    for i=3:indis
        sonuc=ikiOncekiSayi+birOncekiSayi;
        ikiOncekiSayi=birOncekiSayi;
        birOncekiSayi=sonuc;
    end
end