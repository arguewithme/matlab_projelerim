function sonuc=SonluAritmetikDizi(A)
diziBoyutu=length(A);
sonuc=1;
fark=A(2)-A(1);
for i=3:diziBoyutu
    if (A(i)-A(i-1))~=fark
        sonuc=0;
        break;
    end
end