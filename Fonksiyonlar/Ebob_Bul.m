function obeb=Ebob_Bul(a,b)
obeb=1;
if (a>b)
    kucuk=a;
    buyuk=b;
else
    kucuk=b;
    buyuk=a;
end
for i=1:kucuk+1
    if (a/i*i==a) && (b/i*i==b)
        obeb=obeb*i;
        a=a/i;
        b=b/i;
    end
end
end