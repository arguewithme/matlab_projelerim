function sonuc=Kombinasyon(m,n)
sonuc=FaktoriyelHesapla(m)/(FaktoriyelHesapla(n)*FaktoriyelHesapla(m-n));