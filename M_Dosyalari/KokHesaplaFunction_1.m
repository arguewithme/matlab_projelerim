function IkinciDereceDenkleminKokleriniHesapla(A,B,C)
delta=B^2-4*A*C;
if delta<0
          disp('Kökler Sanal');
else
          x1=(-B+sqrt(delta))/(2*A);
          x2=(-B-sqrt(delta))/(2*A);
          fprintf('1. Kök : %f \n', x1);
          fprintf('2. Kök : %f \n', x2);
end
